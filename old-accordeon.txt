<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Link to the compiled scss-->
    <link rel="stylesheet" type="text/css" href="../build/styles.css">
    <!--For socials networks-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <title>DOOM - Accordions & tabs</title>
</head>

<body>
    <header>
        <nav class="menu">
            <a class="logo" href="../index.html"><img src="../src/img/logo.png"></a>

            <div class="menu-right-side">
                <ul>
                    <li><a href="../index.html">Text columns</a></li>
                    <li><a href="./typography.html">Typography</a></li>
                    <li><a href="./headlines.html">Headlines</a></li>
                    <li><a href="./buttons.html">Buttons</a></li>
                    <li><a href="./gradients.html">Gradients</a></li>
                    <li><a class="--active" href="./accordions_tabs.html">Accordions & tabs</a></li>
                    <li><a href="./message_boxes.html">Message boxes</a></li>
                    <li><a href="./text_pictures.html">Text & pictures</a></li>
                </ul>
                <div class="call"><a href="callto:+34 657 3556 778"><img src="../src/img/phone-call.png"></a></div>
                <div class="phone-number">+34 657 3556 778</div>
            </div> <!-- /Menu right side -->
        </nav>
    </header><!-- /header -->

    <main>
        <div class="pageTitle">
            <span>Accordions & tabs</span>
        </div>

        <div class="container">
            <!--1ST ROW-->
            <div class="row tabs-1">
                <div class="col-md-3">
                    <ul>
                        <li>Managment</li>
                        <li class="active">Tutorials</li>
                        <li>Video Promotions</li>
                        <li>SEO & Content</li>
                        <li>Video Promotions</li>
                        <li>SEO & Content</li>
                    </ul>
                </div>
                <div class="col-md-9 tabs-content">
                    <div class="col-md-6">
                        <span>about us</span>
                        <h2>How to bring change into you business</h2>
                        <p>
                            Etiam nec odio vestibulum est mattis effic iturut magna.
                            Pellentesque sit amet tellus blandit.
                            Etiam nec odioattis effic vestibul.
                            Etiam nec odio vestibulum est mattis effic iturut magna.
                            Pellentesque sit amet tellus blandit. Etiam nec odio vestibul.
                            Nec odio vestibulum est mattis effic iturut magna.
                            Pellente sque sit amet tellus blandit. Etiam nec odioattis effic vestibul.
                            This is us.
                        </p>
                    </div>
                    <div class="col-md-6 align-self-end">
                        <figure>
                            <img src="https://source.unsplash.com/random/360x215">
                        </figure>
                    </div>
                </div>
            </div>

            <!--2ND ROW-->
            <div class="row tabs-2">
                <div class="col-md-2">
                    <ul>
                        <li>Managment</li>
                        <li class="active">Tutorials</li>
                        <li>Video Promotions</li>
                        <li>SEO & Content</li>
                        <li>Video Promotions</li>
                        <li>SEO & Content</li>

                    </ul>
                </div>
                <div class="col-md-10 tabs-content">
                    <div class="col-md-6 align-self-end">
                        <span>about us</span>
                        <h2>How to bring change into you business</h2>
                        <p>
                            Etiam nec odio vestibulum est mattis effic iturut magna.
                            Pellentesque sit amet tellus blandit.
                            Etiam nec odioattis effic vestibul.
                            Etiam nec odio vestibulum est mattis effic iturut magna.
                            Pellentesque sit amet tellus blandit. Etiam nec odio vestibul.
                            Nec odio vestibulum est mattis effic iturut magna.
                            Pellente sque sit amet tellus blandit. Etiam nec odioattis effic vestibul.
                            This is us.
                        </p>
                    </div>
                    <div class="col-md-6 align-self-end">
                        <p>
                            Etiam nec odio vestibulum est mattis effic iturut magna.
                            Pellentesque sit amet tellus blandit.
                            Etiam nec odioattis effic vestibul.
                            Etiam nec odio vestibulum est mattis effic iturut magna.
                            Pellentesque sit amet tellus blandit. Etiam nec odio vestibul.
                            Nec odio vestibulum est mattis effic iturut magna.
                            Pellente sque sit amet tellus blandit. Etiam nec odioattis effic vestibul.
                            This is us.
                        </p>
                    </div>
                </div>
            </div>

            <!--3TH ROW-->
            <div class="row tabs-3">
                <div class="col-md-11">
                    <ul>
                        <li class="col-md-2">Managment</li>
                        <li class="active col-md-2">Tutorials</li>
                        <li class="col-md-2">Video Promotions</li>
                        <li class="col-md-2">SEO & Content</li>
                        <li class="col-md-2">Video Promotions</li>
                        <li class="col-md-2">SEO & Content</li>
                    </ul>
                </div>

                <div class="col-md-12 tabs-content">
                    <div class="col-md-6">
                        <span>about us</span>
                        <h2>How to bring change into you business</h2>
                        <p>
                            Etiam nec odio vestibulum est mattis effic iturut magna.
                            Pellentesque sit amet tellus blandit.
                            Etiam nec odioattis effic vestibul.
                            Etiam nec odio vestibulum est mattis effic iturut magna.
                            Pellentesque sit amet tellus blandit. Etiam nec odio vestibul.
                            Nec odio vestibulum est mattis effic iturut magna.
                            Pellente sque sit amet tellus blandit. Etiam nec odioattis effic vestibul.
                            This is us.
                        </p>
                    </div>
                    <div class="col-md-6 align-self-end ">
                        <p>
                            Etiam nec odio vestibulum est mattis effic iturut magna.
                            Pellentesque sit amet tellus blandit.
                            Etiam nec odioattis effic vestibul.
                            Etiam nec odio vestibulum est mattis effic iturut magna.
                            Pellentesque sit amet tellus blandit. Etiam nec odio vestibul.
                            Nec odio vestibulum est mattis effic iturut magna.
                            Pellente sque sit amet tellus blandit. Etiam nec odioattis effic vestibul.
                            This is us.
                        </p>
                    </div>
                </div>
            </div>

            <!--4TH ROW-->
            <div class="row tabs-4">
                <div class="col-md-11 accordion">
                    <div>
                        <ul>
                            <li class="col-md-2">Managment</li>
                            <li class="active col-md-2">Tutorials</li>
                            <li class="col-md-2">Video Promotions</li>
                            <li class="col-md-2">SEO & Content</li>
                            <li class="col-md-2">Video Promotions</li>
                            <li class="col-md-2">SEO & Content</li>
                        </ul>
                    </div>
                </div>

                <div class="tab-title">
                    <span>about us</span>
                    <h2>How to bring change into you business</h2>
                </div>

                <div class="col-md-12 tabs-content">
                    <div class="col-md-6">
                        <p>
                            Etiam nec odio vestibulum est mattis effic iturut magna.
                            Pellentesque sit amet tellus blandit.
                            Etiam nec odioattis effic vestibul.
                            Etiam nec odio vestibulum est mattis effic iturut magna.
                            Pellentesque sit amet tellus blandit. Etiam nec odio vestibul.
                            Nec odio vestibulum est mattis effic iturut magna.
                            Pellente sque sit amet tellus blandit. Etiam nec odioattis effic vestibul.
                            This is us.
                        </p>
                    </div>
                    <div class="col-md-6">
                        <p>
                            Etiam nec odio vestibulum est mattis effic iturut magna.
                            Pellentesque sit amet tellus blandit.
                            Etiam nec odioattis effic vestibul.
                            Etiam nec odio vestibulum est mattis effic iturut magna.
                            Pellentesque sit amet tellus blandit. Etiam nec odio vestibul.
                            Nec odio vestibulum est mattis effic iturut magna.
                            Pellente sque sit amet tellus blandit. Etiam nec odioattis effic vestibul.
                            This is us.
                        </p>
                    </div>
                </div>
            </div><!-- End of row -->

            <!-- 5TH ROW -->
            <div class="row tab-5">
                <div class="col-xs-6 tabs-menu">
                    <ul>
                        <li class="col-md-4">Managment</li>
                        <li class="col-md-4">Tutorials</li>
                        <li class="col-md-4 active">Vidéo Promotions</li>
                    </ul>
                    <div class="tabs-content">
                        <span>about us</span>
                        <h2>How to bring change into you business</h2>
                        <p>
                            Etiam nec odio vestibulum est mattis effic iturut magna.
                            Pellentesque sit amet tellus blandit.
                            Etiam nec odioattis effic vestibul.
                            Etiam nec odio vestibulum est mattis effic iturut magna.
                            Pellentesque sit amet tellus blandit.
                            Etiam nec odio vestibul. Nec odio vestibulum est mattis.
                        </p>
                    </div>
                </div>

                <div class="col-xs-6 ">
                    <ul>
                        <li class="col-md-4">Managment</li>
                        <li class="col-md-4">Tutorials</li>
                        <li class="col-md-4 active with-border">Vidéo Promotions</li>
                    </ul>
                    <div class="tabs-content with-border">
                        <span>about us</span>
                        <h2>How to bring change into you business</h2>
                        <div class="row">
                            <div class="col-md-7 align-self-start">
                                <p>
                                    Etiam nec odio vestibulum est mattis effic iturut magna.
                                    Pellentesque sit amet tellus blandit. 
                                    Etiam nec odioattis effic vestibul. 
                                    Etiam nec odio vestibulum est mattis effic iturut.
                                </p>
                            </div>
                            <div class="col-md-5 align-self-end">
                                <figure>
                                    <img src="https://source.unsplash.com/random/185x155">
                                </figure>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--End container-->
        <section class="tab-6">
            <div class="container container--fluid">
                <ul>
                    <li>Managment</li>
                    <li>Tutorials</li>
                    <li>Video Promotions</li>
                    <li>SEO & Content</li>
                    <li>Video Promotions</li>
                </ul>
            </div>
        </section>

    </main>
    <!-- End main -->

    <footer>
        <div class="container">
            <div class="content_footer">
                <div class="brand">
                    <img src="../src/img/milo.png">
                    <span>2017 All Rights Reserved</span>
                </div>

                <span class="networks">
                    <a href="#"><i class="fab fa-pinterest"></i></a>
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-dribbble"></i></a>
                    <a href="#"><i class="fab fa-behance"></i></a>
                    <a href="#"><i class="fab fa-linkedin-in"></i></a>
                </span>
            </div>
            <!--End of content-footer-->
        </div>
        <!--End of container-->
    </footer>
</body>

</html>


.tabs-1 {
    @include tab();
}

.tabs-2 {
    @include tab();
    li { padding: 15px;}
}

.tabs-3{
    @include tab();

    ul{
        display: flex;

        li { & + li {margin-top: 0;} }
    }

    .tabs-content{margin-top: 75px;}
}

.tabs-4 {
    @include tab();
    .accordion {
        background: #f1f6f9; 
        padding: 20px 30px;
        ul{
            display: flex;
            
            li { 
                background: white;
                &.active { background: #6270ff; }
                & + li { margin-top: 0;} 
                &:hover { background:lightblue; }
            }
        }
    }

    .tab-title{
        margin: 0 auto;


        span {
            text-transform: uppercase;
            font-size: 1.2em;
            color: #838383;
            text-align: center;
            margin-top: 100px;
        }
        
        h2 { margin-top: 20px;}
    }

    .tabs-content{ 
        margin-top: 75px;
    }
}

.tab-5{
    @include tab();
    .tabs-menu {
        display: flex;
        flex-direction: column;
    }
    ul{
        display: flex;
        justify-content: space-around;


        li { 
            color: white;
            background: #6270ff;
            padding: 20px;
            text-align:center;            

            & + li { margin-top: 0; } // To align

            &.active { 
                background: #f1f6f9; 
                color: black;
            }

            &.with-border {
                border: 2px solid #dee6ea;
                border-bottom: none;
            }
        } 
    }
    .tabs-content{
        padding: 65px 50px 0 50px;
        background: #f1f6f9;
        display: flex;
        flex-direction: column;
        flex-grow: 1;
        
        &.with-border { 
            border: 2px solid #dee6ea;
            //border-top: none;
        };

        .row {
            margin-bottom: 0;
        }
    }
}

.tab-6 {
    @include tab();

    background: black;
    color: white;

    ul{
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        align-items: center;

        
        li{
            background: none;

            & + li { margin-top: 0; } // To align
            border: 1px solid yellow;
        }

        
    }
}
